package ru;

import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;

@RestController
public class UserRegistratorResource {
    private final InMemoryUserDetailsManager inMemoryUserDetailsManager;
    private final SecureRandom random;
    private final User.UserBuilder userBuilder;

    public UserRegistratorResource(InMemoryUserDetailsManager inMemoryUserDetailsManager,
                                   SecureRandom random,
                                   User.UserBuilder userBuilder) {
        this.inMemoryUserDetailsManager = inMemoryUserDetailsManager;
        this.random = random;
        this.userBuilder = userBuilder;
    }

    @PostMapping("/password")
    public String generatePassword() {
        byte[] bytes = new byte[10];
        random.nextBytes(bytes);
        return new String(bytes, StandardCharsets.UTF_16);
    }

    @PostMapping(value = "/user", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public void registerUser(@RequestParam("username") String username,
                               @RequestParam("password") String password) {

        UserDetails user = userBuilder.username(username).password(password).roles("USER").build();
        inMemoryUserDetailsManager.createUser(user);
    }
}

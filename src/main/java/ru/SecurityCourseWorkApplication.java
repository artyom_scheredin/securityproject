package ru;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityCourseWorkApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityCourseWorkApplication.class, args);
	}

}
